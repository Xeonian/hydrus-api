#!/usr/bin/env python3

# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import pathlib
import enum
import typing as T

import hydrus_api
import hydrus_api.utils

REQUIRED_PERMISSIONS = (hydrus_api.Permission.SEARCH_FILES,)


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("api_key")
    parser.add_argument("tags", nargs="+")
    parser.add_argument("--destination", "-d", type=pathlib.Path, default=pathlib.Path.cwd())
    parser.add_argument("--write-metadata", "-m", action="store_true")
    parser.add_argument("--limit", "-l", type=int, default=None)
    parser.add_argument("--chunk-size", "-c", type=int, default=100)
    parser.add_argument("--file-service", "-f", dest="file_services", action="append")
    parser.add_argument("--tag-service", "-t", default="my tags")
    parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)
    return parser


def main(arguments) -> int:
    client = hydrus_api.Client(arguments.api_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ExitCode.FAILURE

    # Translate passed file- and tag-service keys or names into keys. If there are multiple services with the same name
    # we just take the first one
    service_mapping = hydrus_api.utils.get_service_mapping(client)
    if arguments.file_services:
        file_service_keys = [
            service_mapping[name_or_key][0] if name_or_key in service_mapping else name_or_key
            for name_or_key in arguments.file_services
        ]
    else:
        file_service_keys = None
    tag_service_key = (
        service_mapping[arguments.tag_service][0]
        if arguments.tag_service in service_mapping
        else arguments.tag_service_key
    )

    all_file_ids = client.search_files(
        tags=arguments.tags,
        file_service_keys=file_service_keys,
        tag_service_key=tag_service_key,
    )["file_ids"]
    exported = 0
    print(f"Exporting {arguments.limit or len(all_file_ids)} files to {arguments.destination}...")
    for file_ids in hydrus_api.utils.yield_chunks(all_file_ids, arguments.chunk_size):
        metadata = client.get_file_metadata(file_ids=file_ids)["metadata"]
        for metadatum in metadata:
            if arguments.limit is not None and exported >= arguments.limit:
                return ExitCode.SUCCESS

            mime_type = metadatum["mime"]
            extension = "." + mime_type.rsplit("/", 1)[1] if "/" in mime_type else ""
            path = T.cast(pathlib.Path, arguments.destination) / f"{metadatum['hash']}{extension}"
            path.write_bytes(client.get_file(file_id=metadatum["file_id"]).content)

            if arguments.write_metadata:
                tags = metadatum["tags"][tag_service_key]["storage_tags"][str(hydrus_api.TagStatus.CURRENT)]
                metadata_path = path.with_name(f"{path.name}.txt")
                metadata_path.write_text("\n".join(sorted(tags)), hydrus_api.utils.HYDRUS_METADATA_ENCODING)

            exported += 1

    return ExitCode.SUCCESS


if __name__ == "__main__":
    parser = get_argument_parser()
    arguments = parser.parse_args()
    parser.exit(main(arguments))
