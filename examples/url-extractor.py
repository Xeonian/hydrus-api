#!/usr/bin/env python3

# Copyright (C) 2023 cryzed
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import re
import enum

import hydrus_api
import hydrus_api.utils

REQUIRED_PERMISSIONS = (hydrus_api.Permission.SEARCH_FILES,)


class ExitCode(enum.IntEnum):
    SUCCESS = 0
    FAILURE = 1


def get_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("api_key")
    parser.add_argument("regex")
    parser.add_argument("tags", nargs="+")
    parser.add_argument("--chunk-size", "-c", type=int, default=100)
    parser.add_argument("--api-url", "-a", default=hydrus_api.DEFAULT_API_URL)
    return parser


def main(arguments: argparse.Namespace) -> ExitCode:
    client = hydrus_api.Client(arguments.api_key, arguments.api_url)
    if not hydrus_api.utils.verify_permissions(client, REQUIRED_PERMISSIONS):
        print("The API key does not grant all required permissions:", REQUIRED_PERMISSIONS)
        return ExitCode.FAILURE

    regex = re.compile(arguments.regex)
    file_ids = client.search_files(arguments.tags)["file_ids"]
    for file_ids in hydrus_api.utils.yield_chunks(file_ids, arguments.chunk_size):
        metadata = client.get_file_metadata(file_ids=file_ids)["metadata"]
        for metadatum in metadata:
            for url in metadatum["known_urls"]:
                if regex.match(url):
                    print(url)

    return ExitCode.SUCCESS


if __name__ == "__main__":
    parser = get_argument_parser()
    arguments = parser.parse_args()
    parser.exit(main(arguments))
